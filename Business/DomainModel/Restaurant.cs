﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Data;

namespace Business.DomainModel
{
    public class Restaurant
    {
        public int Id { get; set; }

        [Required]
        [StringLength(500)]
        public string Title { get; set; }

        /// <summary>
        /// Should always be stored in format: (00) 0000 0000
        /// </summary>
        [Required]
        [DataType(DataType.PhoneNumber)] 
        [ValidPhoneNumber]
        public string PhoneNumberText { get; set; }

        /// <summary>
        /// Phone number as entered into UI is converted to a long here, for use as a natural key
        /// </summary>
        public long PhoneNumberKey { get; set; }

        public CuisineEnum Cuisine { get; set; }
        public string HeadChefName { get; set; }
        
        /// <summary>
        /// "star-ratings" for food/wine must be in range 0 - 3
        /// </summary>
        public byte? RatingFood { get; set; }
        public byte? RatingWine { get; set; }

        public string StreetAddress { get; set; }
        public string AddressSuburb { get; set; }
        public string AddressState { get; set; }
        public string AddressPostCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string WebAddress { get; set; }
        public string ReviewText { get; set; }

        public Restaurant()
        {

        }

        public Restaurant(Data.Restaurant dRest)
        {
            //I'd rather define Business.DomainModel.Restaurant as a partial class of Data.Restaurant
            //to avoid his instantiation step, but i'm doing it this way for expediency....

            this.Id = dRest.Id;
            this.Title = dRest.Title;
            this.PhoneNumberText = dRest.PhoneNumberText;
            this.PhoneNumberKey = dRest.PhoneNumberKey;
            //add the rest of the Restaurant properties here...
        }

        public void ParsePhoneNumber()
        {
            //Parse the PhoneNumber according to the rules

            Regex rgx = new Regex("[^0-9]");
            string str = rgx.Replace(this.PhoneNumberText.ToString(), "");

            long phoneNumberKey;
            if (!long.TryParse(str, out phoneNumberKey)) throw new ApplicationException("Unable to update restaurant. Invalid phone number.");
            if (phoneNumberKey.ToString().Length < 9) throw new ApplicationException("Unable to update restaurant. Invalid phone number.");
            if (phoneNumberKey.ToString().Length > 9)
            {
                str = str.Substring(0, 9);
                if (!long.TryParse(str, out phoneNumberKey)) throw new ApplicationException("Unable to update restaurant. Invalid phone number.");
            }
            
            this.PhoneNumberKey = phoneNumberKey;
            this.PhoneNumberText = GetPhoneNumberText(PhoneNumberKey);
        }

        private string GetPhoneNumberText(long number)
        {
            //Get the phone number formated as (00) 0000 0000
            string str = number.ToString();
            if (str.Length != 9) throw new ApplicationException("Unable to update restaurant. Invalid phone number.");

            string n1 = str.Substring(0, 1);
            string n2 = str.Substring(1, 4);
            string n3 = str.Substring(5, 4);

            return string.Format("(0{0}) {1} {2}", n1, n2 ,n3);
        }
    }

    public class ValidPhoneNumber : ValidationAttribute
    {
        //Custom validation for the phone number

        public ValidPhoneNumber()
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //given time I would create common code for this custom validator & for the ParsePhoneNumber method.

            Regex rgx = new Regex("[^0-9]");
            string str = rgx.Replace(value.ToString(), "");

            long phoneNumberKey;
            if (!long.TryParse(str, out phoneNumberKey)) return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));

            if (phoneNumberKey.ToString().Length < 9) return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));

            return null;
        }
    }
}
