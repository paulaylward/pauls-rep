﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using System.Text.RegularExpressions;

namespace Business.DomainModel
{
    public class DataContext : IDisposable
    {
        //protected static string conn = System.Configuration.ConfigurationManager.ConnectionStrings["CoinExpress"].ConnectionString;

        protected Data.DatabaseDataContext _db;

        public DataContext(string dataConnection)
        {
            //dependency injection
            _db = new Data.DatabaseDataContext(dataConnection);
        }

        public List<Restaurant> GetRestaurants()
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            foreach (Data.Restaurant dRest in _db.Restaurants.ToList())
            {
                restaurants.Add(new Restaurant(dRest));
            }

            return restaurants;
        }

        public Restaurant GetRestaurant(int id)
        {
            Data.Restaurant dRest = _db.Restaurants.Where(r => r.Id == id).FirstOrDefault();
            if (dRest != null) return (new Restaurant(dRest));
            else return null;
        }

        public void UpdateRestaurant(Restaurant restaurant)
        {
            _db.UpdateRestaurant(restaurant.Id, restaurant.Title, restaurant.PhoneNumberKey, restaurant.PhoneNumberText);
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
