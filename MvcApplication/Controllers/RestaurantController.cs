﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.DomainModel;
using System.Net;

namespace MvcApplication.Controllers
{
    public class RestaurantController : Controller
    {
        //
        // GET: /Restaurant/

        private ActionResult LoadRestaurant(int id)
        {
            //strongly typed view returned
            Business.DomainModel.DataContext db = new Business.DomainModel.DataContext(ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString);
            
            Restaurant restaurant = db.GetRestaurant(id);
            if (restaurant == null) return HttpNotFound();
            else return View(restaurant);
        }

        public ActionResult Edit(int id)
        {
            //strongly typed view returned
            Business.DomainModel.DataContext db = new Business.DomainModel.DataContext(ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString);

            Restaurant restaurant = db.GetRestaurant(id);
            if (restaurant == null) return HttpNotFound();
            else return View(restaurant);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,FirstName,PhoneNumberText")] Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                Business.DomainModel.DataContext db = new Business.DomainModel.DataContext(ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString);
                restaurant.ParsePhoneNumber();
                db.UpdateRestaurant(restaurant);
                ViewBag.Message = String.Format("Saved '{0}'.", restaurant.Title);
                ModelState.Clear();
            }
            return View(restaurant);
        }
    }
}
