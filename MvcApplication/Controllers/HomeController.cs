﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication.Models;
using Business;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //strongly typed view returned
            Business.DomainModel.DataContext db = new Business.DomainModel.DataContext(ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString);
            
            return View(db.GetRestaurants());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
